<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Forms;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Menu\Builder;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $user = $this->getUser();

        $menu = (new \App\Menu\Builder)->mainMenu($this->isGranted('ROLE_USER'), $user);

        $rotationClient = HttpClient::create();
        $rotation = $rotationClient->request('GET', 'https://euw1.api.riotgames.com/lol/platform/v3/champion-rotations?api_key=' . API_KEY)->toArray();

        $championsClient = HttpClient::create();
        $champions = $championsClient->request('GET', 'http://ddragon.leagueoflegends.com/cdn/10.10.3208608/data/fr_FR/champion.json')->toArray();

        $freeChampions = [];

        foreach ($champions['data'] as $champion) {
            if (in_array($champion['key'], $rotation['freeChampionIds'])) {
                array_push($freeChampions, $champion);
            }
        }

        return $this->render('home/index.html.twig', [
            'free_champions' => $freeChampions,
            'menu' => $menu,
        ]);
    }
}
