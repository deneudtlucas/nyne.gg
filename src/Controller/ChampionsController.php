<?php

namespace App\Controller;

use App\Menu\Builder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Routing\Annotation\Route;

class ChampionsController extends AbstractController
{
    /**
     * @Route("/champions", name="champions")
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $user = $this->getUser();

        $menu = (new \App\Menu\Builder)->mainMenu($this->isGranted('ROLE_USER'), $user);

        $championsClient = HttpClient::create();
        $champions = $championsClient->request('GET', 'http://ddragon.leagueoflegends.com/cdn/10.10.3208608/data/en_US/champion.json')->toArray();

        $championsPaginated = $paginator->paginate($champions['data'], $request->query->getInt('page', 1));

        return $this->render('champions/index.html.twig', [
            'image_champions_paginated' => $championsPaginated,
            'menu' => $menu,
        ]);
    }
}
