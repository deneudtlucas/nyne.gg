<?php

namespace App\Controller;

use App\Menu\Builder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;

class ChampionController extends AbstractController
{
    /**
     * @Route("/champion/{championName}", name="champion")
     */
    public function index($championName)
    {
        $user = $this->getUser();

        $menu = (new \App\Menu\Builder)->mainMenu($this->isGranted('ROLE_USER'), $user);

        $championsClient = HttpClient::create();
        $champions = $championsClient->request('GET', 'http://ddragon.leagueoflegends.com/cdn/10.10.3208608/data/fr_FR/champion.json')->toArray();

        $champion = $champions['data'][str_replace(' ', '', $championName)];

        return $this->render('champion/index.html.twig', [
            'champion' => $champion,
            'menu' => $menu,
        ]);
    }
}
