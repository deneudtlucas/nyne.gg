<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;

class InvocatorController extends AbstractController
{
    /**
     * @Route("/invocator/{username}", name="invocator")
     * @param $username
     */
    public function index($username)
    {
        $user = $this->getUser();

        $menu = (new \App\Menu\Builder)->mainMenu($this->isGranted('ROLE_USER'), $user);

        $invocatorClient = HttpClient::create();
        $invocatorEncrypted = $invocatorClient->request('GET', 'https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/' . $username . '?api_key=' . API_KEY)->toArray();

        $invocator = $invocatorClient->request('GET', 'https://euw1.api.riotgames.com/lol/league/v4/entries/by-summoner/' . $invocatorEncrypted['id'] . '?api_key=' . API_KEY)->toArray();

        return $this->render('invocator/index.html.twig', [
            'icon' => $invocatorEncrypted['profileIconId'],
            'invocatorQueue1' => $invocator[0],
            'invocatorQueue2' => $invocator[1],
            'menu' => $menu,
        ]);
    }
}
