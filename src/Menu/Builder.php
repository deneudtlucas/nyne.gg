<?php

namespace App\Menu;

use App\Entity\User;
use Knp\Menu\Matcher\Matcher;
use Knp\Menu\MenuFactory;
use Knp\Menu\Renderer\ListRenderer;

class Builder
{
    public function mainMenu($isGranted, $user)
    {
        $factory = new MenuFactory();
        $menu = $factory->createItem('Menu');
        if ($user == null) {
            $menu->addChild('Login', ['uri' => '/login']);
        } else {
            $menu->addChild('Logout', ['uri' => '/logout']);
        }
        $menu->addChild('Home', ['uri' => '/']);
        $menu->addChild('Champions', ['uri' => '/champions']);
        if ($isGranted && $user != null) {
            $menu->addChild('See Summoner Page', ['uri' => '/invocator/' . $user->getUsername()]);
        }
        $menu->addChild('Search for an Invocator', ['uri' => '/search']);

        $menu->setChildrenAttribute('class', 'menu');

        $renderer = new ListRenderer(new Matcher());
        return $renderer->render($menu);
    }
}